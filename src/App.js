import logo from './logo.svg';
import './App.css';
import InfoPage from './Pages/InfoPage';

function App() {
  return (
    <div className="App">
      <InfoPage/>
    </div>
  );
}

export default App;
