import React, { useEffect, useState} from 'react'
import { getInfo } from '../../Api/Api';
import './Average.scss'


function Average() {
    const [info, setInfo]= useState(null)
    const [average, setAverage]= useState(null)
    const [price, setPrice]= useState(null)


    const getAverage =(array) =>{
        let sum=0;
        for(let item of array){
            sum+=item;
        }
        const len= array.length;
        return sum/len
    }

    const getPrice =() =>{
        if(info && average){
            const n = info[0].wind10m
            if(n.direction=='N'){
                return n.speed*average*2/10
            }else if(n.direction=='S'){
                return n.speed*average*1.5/10
            }
        }
        

    }

    const getInfoApi= async()=>{
        try{
            const infoApi= await getInfo()
            setInfo(infoApi.dataseries)
            const aux= infoApi.dataseries.slice(0,10).map((item)=>item.wind10m.speed)
            setAverage(getAverage(aux));
            
            
        }catch(error){
            console.error(error)
        }
    }

    useEffect(()=>{
        setPrice(getPrice())
    },[average,info])

    useEffect(()=>{
        getInfoApi()
    },[])

    return (
        <div className='c-average'>
            <div className='c-average__div'>
                <h2>Media de viento (últimos 10)</h2>
                <p>{average}</p>
                

                <h2>Precios esperados</h2>
                <p>{price }</p>
            </div>
            
        </div>
    )
}

export default Average
